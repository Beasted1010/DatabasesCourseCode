import java.sql.*;
import org.apache.derby.jdbc.ClientDriver;
import java.util.Scanner;
import java.util.ArrayList;
import java.text.SimpleDateFormat;



// TODO: Spacing will need to be adjusted once we run it and can properly format the output



public class ats
{
	public static void main(String[] args)
	{
		int choice;
		Connection conn = null;
		try
		{
			// Get a client object and connect to the database server
			Driver d = new ClientDriver();
            String url = "jdbc:derby://localhost:2460/atscheduler" + ";create=false";
			conn = d.connect(url, null);
			
			// Make a menu selection
			choice = 1;                         // MENU GOES HERE
			switch (choice)
			{
				case 1: break;
				default: System.out.println("Illegal choice");
						 break;
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			// Disconnect from the server
			try
			{
				if(conn != null)
				conn.close();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	// This method is for the query for the number of available nodes.
	public static void NumberOfAvailableNodesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select count(n.name) as NumNodes "
						+ "from Nodes n "
						+ "where n.available = True "
						+ "group by n.available";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("NumNodes");
		while (rs.next())
		{
			int numNodes = rs.getInt("NumNodes");
			System.out.println(numNodes);
		}
		System.out.println( );
		rs.close();
	}


	// This method is for the query for the node names that are available.
	public static void AvailableNodesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select n.available as NodeName "
						+ "from Nodes n "
						+ "where n.available = True ";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("NodeName");
		while (rs.next())
		{
			String nodeName = rs.getString("NodeName");
			System.out.println(nodeName);
		}
		System.out.println( );
		rs.close();
	}


	// This method is for the query for the information regarding a user specified node.
	public static void NodeInformationInputQuery(Connection conn) throws SQLException
	{
		// Build and execute the query
		String nodeName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter a node name: ");
		nodeName = scannerObject.nextLine( );
		Statement stmt = conn.createStatement();
		String qry = "select Name as NodeName, Type, Location, Model, TenG_IP, OneG_IP, iDRAC_IP, CacheCard, Available "
						+ "from Nodes "
						+ "where NodeName = ’" + nodeName + "’";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		// System.out.format("%-10s %3s %-12s %-%n", "NodeName", "Type", "Location"); -> May want something like this that is well formatted. There is a lot of output to the screen that may make formatting difficult
		System.out.println("NodeName\tType\tLocation\tModel\tTenG_IP\tOneG_IP\tiDRAC_IP\tCacheCard\tAvailable");
		while (rs.next())
		{
			String nodeNameOut = rs.getString("NodeName");
			String type = rs.getString("Type");
			String location = rs.getString("Location");
			String model = rs.getString("Model");
			String tenG_IP = rs.getString("TenG_IP");
			String oneG_IP = rs.getString("OneG_IP");
			String iDRAC_IP = rs.getString("iDRAC_IP");
			String cacheCard = rs.getString("CacheCard");
			String available = rs.getString("Available");
			System.out.println( nodeNameOut + "\t" + type + "\t" + location + "\t" + model + "\t" + tenG_IP + "\t" + oneG_IP + "\t" + iDRAC_IP + "\t" + cacheCard + "\t" + available );
		}
		rs.close();
	}



	// This method is for the query for information regarding a specific test case
	public static void TestCaseInformationQuery(Connection conn) throws SQLException
	{
		String testCaseName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter a test case name: ");
		testCaseName = scannerObject.nextLine( );
		
		Statement stmt = conn.createStatement();
		String qry = "select CID, Name as TestCaseName, SID, EstimatedRuntime "
						+ "from TestCases "
						+ "where Name = " + testCaseName;
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("CID\tTestCaseName\tSID\tEstimatedRuntime");
		while (rs.next())
		{
			int cID = rs.getInt("CID");
			String testCaseNameOut = rs.getString("TestCaseName");
			int sID = rs.getInt("SID");
			java.sql.Time estimatedRuntime = rs.getTime("EstimatedRuntime"); // TODO: This may need a better data type
			System.out.println( Integer.toString(cID) + "\t" + testCaseNameOut + "\t" + Integer.toString(sID) + "\t" + estimatedRuntime.toString() );
		}
		System.out.println( );
		rs.close();
	}



	// This method is for the query for the number of passing test cases
	public static void NumberOfPassingTestCasesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select count(TestResultID) as NumPassingTestCases "
						+ "from TestResults "
						+ "where status = 'passed' "
						+ "group by status";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("NumPassingTestCases");
		while (rs.next())
		{
			int numPassingTestCases = rs.getInt("NumPassingTestCases");
			System.out.println( Integer.toString(numPassingTestCases) );
		}
		System.out.println( );
		rs.close();
	}


	// This method is for the query for the number of running test suites
	public static void NumberOfRunningTestSuitesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select count(TID) as NumRunningTestSuites "
						+ "from TestExecutionSets "
						+ "where IsRunning = True "
						+ "group by IsRunning";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("NumRunningTestSuites");
		while (rs.next())
		{
			int numRunningTestSuites = rs.getInt("NumRunningTestSuites");
			System.out.println( Integer.toString(numRunningTestSuites) );
		}
		System.out.println( );
		rs.close();
	}


	// This method is for the query for information about a node's assignment
	public static void NodeAssignmentInputQuery(Connection conn) throws SQLException
	{
		// Build and execute the query
		String nodeName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter a node name: ");
		nodeName = scannerObject.nextLine( );
		Statement stmt = conn.createStatement();
		
		String qry = "select NodeName, TID as TestSuiteNodeAssignedTo "
						+ "from NodesAssignedTo "
						+ "where NodeName = " + nodeName;
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("NodeName\tTestSuiteNodeAssignedTo\t");
		while (rs.next())
		{
			int testSuiteNodeAssignedTo = rs.getInt("TestSuiteNodeAssignedTo");
			String nodeNameOut = rs.getString("NodeName");
			System.out.println( Integer.toString(testSuiteNodeAssignedTo) + "\t" + nodeNameOut );
		}
		System.out.println( );
		rs.close();
	}


	// This method is for the query for the available IPs
	public static void AvailableIPQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select IP "
						+ "from IPAddresses "
						+ "where available = True ";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("IP");
		while (rs.next())
		{
			String ip = rs.getString("IP");
			System.out.println(ip);
		}
		System.out.println( );
		rs.close();
	}


	// This method is for the query to list the test cases of a test suite
	public static void ListTestCasesForTestSuiteQuery(Connection conn) throws SQLException
	{
		// Build and execute the query
		String testSuiteName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter in the name of a test suite: ");
		testSuiteName = scannerObject.nextLine( );
		
		Statement stmt = conn.createStatement();
		String qry = "select tc.Name as TestCaseName, tc.EstimatedRuntime as EstimateRunTime "
						+ "from TestSuites ts, TestCases tc "
						+ "where ts.Name = '" + testSuiteName + "' and "
						+ "ts.SID = tc.SID";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.println("TestCaseName\tEstimateRunTime");
		while (rs.next())
		{
			int testCaseName = rs.getInt("TestCaseName");
			java.sql.Time estimatedRuntime = rs.getTime("EstimatedRuntime"); // TODO: This may need a better data type
			System.out.println( testCaseName + "\t" + estimatedRuntime.toString() );
		}
		System.out.println( );
		rs.close();
	}






	// BELOW IS A MORE FLEXIBLE MEANS OF QUERYING THE DATABASE




	/* TODO:
			Need to ensure all of my arrays and such are given sizes as appropriate
			Need to ensure that ArrayList is also being initialized well (note that ArrayList<String[]> the String[] can't be given a size?
											See https://stackoverflow.com/questions/1921181/java-arraylist-of-string-arrays for alternative
											
											
			ADD THE FOLLOWING
				- ABILITY TO HANDLE USER INPUT SELECTION QUERIES
				- ABILITY TO HANDLE DELETION QUERIES
				- ABILITY TO HANDLE UPDATE QUERIES
				- ABILITY TO HANDLE ORDER BY QUERIES
				
			
			FUTURE DEVELOPMENT
				- Handle the query execution output in a more compact scalable manner?
				- Add quotes around outputted attribute names (for clarity)?
				
				
			POTENTIAL BUGS
				- HANDLE CASE WHERE HELPER FUNCTION RETURN VALUE IS NULL/EMPTY/UNINITIALIZED (i.e. user entered 0 immediately)







	*/





	// -------------------------------------------------------------------- HELPER FUNCTIONS --------------------------------------------------------------------


	// Build up a single string to be used in our query for the group by clause
	public static String FormatGroupByClauseForQuery(ArrayList<String> groupByAttributes)
	{
		StringBuilder resultGroupByString = new StringBuilder();
		
		// Loop through all of our groupByAttributes, format the attribute appropriately for SQL
		// Each groupByAttributes entry has a string representing an attribute
		for( int i = 0; i < groupByAttributes.size(); i++ )
		{
			// If we have more than one attribute in our group by clause, then concatenate the next attribute with a comma -> Need comma separated components in group by clause
			if( i > 0 )
			{
				resultGroupByString.append(", ");
			}
			
			// Format for SQL query is GROUP BY attr1, attr2, ... , so adhere to this format
			resultGroupByString.append(groupByAttributes.get(i));
		}
		
		// We used a StringBuilder object, so now convert what was built into a full fledged string and return that String
		return( resultGroupByString.toString() );
	}



	// NOTE: GROUP BY X -> Means to put all those with the same value for X in the group
	// NOTE: GROUP BY X, Y -> Means to put all those with the same values for BOTH X and Y in the group
	// Grab the attributes to be used in the group by section.
	// Returns an ArrayList of arbitrary size containing all the attributes (as strings) to group by
	public static ArrayList<String> GetGroupByInfo(ArrayList<String[]> attributesAndTypes)
	{
		ArrayList<String> resultGroupByInfo = new ArrayList<String>();
		Scanner keyboardScanner = new Scanner(System.in);
		
		System.out.println("The possible attributes to aggregate over by are: ");
		for( int i = 0; i < attributesAndTypes.size(); i++ )
		{
			System.out.println(" Attribute: " + attributesAndTypes.get(i)[0] + "\tType: " + attributesAndTypes.get(i)[1]);
		}
		
		while( true )
		{
			System.out.print("Enter in the next attribute you wish to group by (enter '0' (zero) to stop): ");
			String attributeChoice = keyboardScanner.nextLine();
			
			int index = -1;
			// Grab the index for which the selected attribute corresponds to, this will be used for adding to the result
			for( int i = 0; i < attributesAndTypes.size(); i++ )
			{
				if( attributesAndTypes.get(i)[0].equals(attributeChoice) )
				{
					index = i;
					break;
				}
			}
			
			// Did the user attempt to enter in an actual attribute
			if( !attributeChoice.equals("0") && !attributeChoice.equals("zero") )
			{
				// The attribute entered was valid
				if( index != -1 )
				{
					// We are now ready to add to the attribute the group by operation
					resultGroupByInfo.add( attributeChoice );
				}
				// The attribute entered was not valid
				else
				{
					System.out.println("Invalid attribute entry! Please try again");
					continue;
				}
			}
			// Break out of loop on the case that user enters in "0" or "zero"
			else
			{
				break;
			}
		}
		
		// Return the resulting array of strings representing attributes
		return(resultGroupByInfo);
	}




	// Build up a single string to be used in our query for the aggregation part of the select clause
	// attributesValuesAndTypes Format: Index = 0: AGGREGATION_OPERATION,   Index = 1: ATTRIBUTE_NAME,   Index = 2: ATTRIBUTE_TYPE
	public static String FormatAggregationOperationsForQuery(ArrayList<String[]> aggregationInfo)
	{
		StringBuilder resultAggregationString = new StringBuilder();
		
		// Loop through all of our aggregation operations, extract the aggregation operation (index=0) and the attribute name (index=1) to be added to resulting string
		// Each aggrgationInfo entry contains AGGREGATION_OPERATION, ATTRIBUTE_NAME, and ATTRIBUTE_TYPE
		for( int i = 0; i < aggregationInfo.size(); i++ )
		{
			// If we have more than one aggregation operation in, then concatenate the next aggregation operation with a comma -> Need comma separated components in select clause
			if( i > 0 )
			{
				resultAggregationString.append(", ");
			}
			
			// Format for SQL query is AGGREGATION_OPERATION( ATTRIBUTE_NAME ), so adhere to this format
			// First comes the AGGREGATION_OPERATION (index=0)
			resultAggregationString.append(aggregationInfo.get(i)[0]);
			
			// Include the ( symbol as indicated in the SQL query format
			resultAggregationString.append("( ");
			
			// Then add on the attribute name
			resultAggregationString.append(aggregationInfo.get(i)[1]);
			
			// Finally add the trailing ) to satisfy the query format
			resultAggregationString.append(" )");
		}
		
		// We used a StringBuilder object, so now convert what was built into a full fledged string and return that String
		return( resultAggregationString.toString() );
	}


	// TODO: FUTURE DEVELOPMENT: Add more aggregation function support
	// Grab the aggregation information over whatever attributes the user desires.
	// resultAggregationInfo Format: Index = 0: AGGREGATION_OPERATION,   Index = 1: ATTRIBUTE_NAME,   Index = 2: ATTRIBUTE_TYPE
	public static ArrayList<String[]> GetAggregationInfo(ArrayList<String[]> attributesAndTypes)
	{
		ArrayList<String[]> resultAggregationInfo = new ArrayList<String[]>();
		Scanner keyboardScanner = new Scanner(System.in);
		
		System.out.println("The possible attributes to aggregate over by are: ");
		for( int i = 0; i < attributesAndTypes.size(); i++ )
		{
			System.out.println(" Attribute: " + attributesAndTypes.get(i)[0] + "\tType: " + attributesAndTypes.get(i)[1]);
		}
		
		// TODO: FUTURE DEVELOPMENT: "PrintAggregationOptions" function?
		String[] aggregationOptions = { "AVG", "COUNT", "MIN", "MAX", "SUM" };
		System.out.println("\n"); // 2 blank lines for neatness
		System.out.println("The possible aggregation functions to use on the above attributes are: ");
		System.out.println(" - AVG \tCalculate the average of a set of values");
		System.out.println(" - COUNT \tCounts rows in a specified table or view");
		System.out.println(" - MIN \tGet the minimum value in a set of values");
		System.out.println(" - MAX \tGet the maximum value in a set of values");
		System.out.println(" - SUM \tCalculates the sum of values");
		
		while( true )
		{
			System.out.print("Enter in the next attribute you wish to aggregate over (enter '0' (zero) to stop): ");
			String attributeChoice = keyboardScanner.nextLine();
			
			int index = -1;
			// Grab the index for which the selected attribute corresponds to, this will be used for adding to the result
			for( int i = 0; i < attributesAndTypes.size(); i++ )
			{
				if( attributesAndTypes.get(i)[0].equals(attributeChoice) )
				{
					index = i;
					break;
				}
			}
			
			String aggregationChoice = ""; // Initialize
            
			// Did the user attempt to enter in an actual attribute
			if( !attributeChoice.equals("0") && !attributeChoice.equals("zero") )
			{
				// The attribute entered was valid
				if( index != -1 )
				{
					// Ensure user enters in a valid aggregation option before continuing
					int found = 0;
					while( found != 1 )
					{
						System.out.print("Enter in the aggregation operation to perform over the attribute '" + attributeChoice + ":");
						aggregationChoice = keyboardScanner.nextLine();
						
						for( int i = 0; i < aggregationOptions.length; i++ )
						{
							// Is the current aggregation option what the user selected as their choice?
							if( aggregationChoice.equals(aggregationOptions[i]) )
							{
								found = 1;
								break;
							}
						}
						
						if( found != 1 )
						{
							System.out.println("Invalid aggregation option of '" + aggregationChoice + "'! Please try again.");
							System.out.print("\n");
						}
					}
					
					// We are now ready to add to our result the desired aggregation operation to perform over the specified attribute
					// Format: AGGREGATION_OPERATION, ATTRIBUTE_NAME, ATTRIBUTE_TYPE
					resultAggregationInfo.add( new String[] {aggregationChoice, attributesAndTypes.get(index)[0], attributesAndTypes.get(index)[1]} );
				}
				// The attribute entered was not valid
				else
				{
					System.out.println("Invalid attribute entry! Please try again");
					continue;
				}
			}
			// Break out of loop on the case that user enters in "0" or "zero"
			else
			{
				break;
			}
		}
		
		// Return the resulting array of (Aggregations, Attributes, and Types)
		return(resultAggregationInfo);
	}



	// Build up a single string to be used in our query for the where clause
	// attributesValuesAndTypes Format: Index = 0: ATTRIBUTE_NAME,   Index = 1: ATTRIBUTE_VALUE,   Index = 2: ATTRIBUTE_TYPE
	public static String FormatFilteredValuesForQuery(ArrayList<String[]> attributesValuesAndTypes)
	{
		StringBuilder resultFilterString = new StringBuilder();
		
		// Loop through all of our attributes, extract the attribute name (index=0) and the attribute value (index=1) to be added to resulting string
		// Ensure that we quote necessary types as indicated by the attribute type (index=2)
		for( int i = 0; i < attributesValuesAndTypes.size(); i++ )
		{
			// If we have more than one attribute/value pair, then concatenate the next attribute/value pair with SQL's "and" keyword
			if( i > 0 )
			{
				resultFilterString.append(" and ");
			}
			
			// Format for SQL query is ATTRIBUTE_NAME = ATTRIBUTE_VALUE, so adhere to this format
			// First comes the ATTRIBUTE_NAME (index=0)
			resultFilterString.append(attributesValuesAndTypes.get(i)[0]);
			// Include the = sign as indicated in the SQL query format
			resultFilterString.append(" = ");
			
			// Then add on the value, but be sure to include quotes as appropriate for the required datatype
			// Types with INT in them (i.e. INT or INTEGER) and types with BOOL in them (i.e. BOOL or BOOLEAN) should not be quoted
			if( attributesValuesAndTypes.get(i)[2].toUpperCase().contains("INT") || attributesValuesAndTypes.get(i)[2].toUpperCase().contains("BOOL") )
			{
				resultFilterString.append(attributesValuesAndTypes.get(i)[1]);
			}
			// All other types that we (currently) care about require quotes around them
			else
			{
				resultFilterString.append("'");
				resultFilterString.append(attributesValuesAndTypes.get(i)[1]);
				resultFilterString.append("'");
			}
		}
		
		// We used a StringBuilder object, so now convert what was built into a full fledged string and return that String
		return( resultFilterString.toString() );
	}


	// Takes in array of arrays of strings that represents the possible attributes a user can filter by
	// Returns a condensed version of this array which contains only the user's selection and corresponding type
	// attributesAndTypes Format: Index = 0: ATTRIBUTE_NAME,   Index = 1: ATTRIBUTE_TYPE
	// resultAttributesValuesAndTypes Format: Index = 0: ATTRIBUTE_NAME,   Index = 1: ATTRIBUTE_VALUE,   Index = 2: ATTRIBUTE_TYPE
	public static ArrayList<String[]> GetFilterValues(ArrayList<String[]> attributesAndTypes)
	{
		ArrayList<String[]> resultAttributesValuesAndTypes = new ArrayList<String[]>();
		Scanner keyboardScanner = new Scanner(System.in);
		
		System.out.println("The possible attributes to filter by are: ");
		for( int i = 0; i < attributesAndTypes.size(); i++ )
		{
			System.out.println(" Attribute: " + attributesAndTypes.get(i)[0] + "\tType: " + attributesAndTypes.get(i)[1]);
		}
		
		while( true )
		{
			System.out.print("Enter in the next attribute you wish to filter by (enter '0' (zero) to stop): ");
			String attributeChoice = keyboardScanner.nextLine();
			
			int index = -1;
			// Grab the index for which the selected attribute corresponds to, this will be used for adding to the result (i.e. we will want the attribute name and type in result)
			for( int i = 0; i < attributesAndTypes.size(); i++ )
			{
				if( attributesAndTypes.get(i)[0].equals(attributeChoice) )
				{
					index = i;
					break;
				}
			}
			
			// Did the user attempt to enter in an actual attribute
			if( !attributeChoice.equals("0") && !attributeChoice.equals("zero") )
			{
				// The attribute entered was valid
				if( index != -1 )
				{
					System.out.println("Please enter in the desired value for " + attributesAndTypes.get(index)[0] + " of type " + attributesAndTypes.get(index)[1]);
					String attributeValue = keyboardScanner.nextLine();
					
					resultAttributesValuesAndTypes.add( new String[] {attributesAndTypes.get(index)[0], attributeValue, attributesAndTypes.get(index)[1]} );
				}
				// The attribute entered was not valid
				else
				{
					System.out.println("Invalid attribute entry! Please try again");
					continue;
				}
			}
			// Break out of loop on the case that user enters in "0" or "zero"
			else
			{
				break;
			}
		}
		
		// Return the resulting array of (Attributes, Values, and Types)
		return(resultAttributesValuesAndTypes);
	}



	// Build up an array of available attributes and their corresponding types
	public static ArrayList<String[]> BuildAttributesAndTypesArray(String tableName)
	{
		ArrayList<String[]> attributesAndTypes = new ArrayList<String[]>();
		
		tableName.toUpperCase();
        
		/*switch(tableName)                                  NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
		{
			case "NODES":
			{
				attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"Type", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"Location", "VARCHAR(20)"} );
				attributesAndTypes.add( new String[] {"Model", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"TenG_IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"OneG_IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"iDRAC_IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"CacheCard", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"Available", "BOOLEAN ('True' or 'False')"} );
			} break;
			
			case "CLIENTS":
			{
				attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"Type", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"Location", "VARCHAR(20)"} );
				attributesAndTypes.add( new String[] {"TenG_IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"OneG_IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"iDRAC_IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"OS", "VARCHAR(20)"} );
				attributesAndTypes.add( new String[] {"Available", "BOOLEAN ('True' or 'False')"} );
			} break;
			
			case "IPADDRESSES":
			{
				attributesAndTypes.add( new String[] {"IP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"Available", "BOOLEAN ('True' or 'False')"} );
			} break;
			
			case "TESTSUITES":
			{
				attributesAndTypes.add( new String[] {"SID", "INT"} );
				attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
			} break;
			
			case "TESTCASES":
			{
				attributesAndTypes.add( new String[] {"CID", "INT"} );
				attributesAndTypes.add( new String[] {"SID", "INT"} );
				attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"MinNumClusters", "INT"} );
				attributesAndTypes.add( new String[] {"MinNumNodes", "INT"} );
				attributesAndTypes.add( new String[] {"MinNumClients", "INT"} );
				attributesAndTypes.add( new String[] {"ClusterOption", "VARCHAR(45)"} );
				attributesAndTypes.add( new String[] {"vNode", "BOOLEAN ('True' or 'False')"} );
				attributesAndTypes.add( new String[] {"EstimatedRunTime", "TIME 'hh:mm:ss'"} );
				attributesAndTypes.add( new String[] {"LastExecution", "TIMESTAMP 'yyyy-mm-dd hh:mm:ss'"} );
			} break;
			
			case "TESTEXECUTIONSETS":
			{
				attributesAndTypes.add( new String[] {"TID", "INT"} );
				attributesAndTypes.add( new String[] {"IsRunning", "BOOLEAN ('True' or 'False')"} );
				attributesAndTypes.add( new String[] {"MVIP", "VARCHAR(15)"} );
				attributesAndTypes.add( new String[] {"SVIP", "VARCHAR(15)"} );
			} break;
			
			case "TESTRESULTS":
			{
				attributesAndTypes.add( new String[] {"TestResultID", "INT"} );
				attributesAndTypes.add( new String[] {"StartTime", "TIMESTAMP 'yyyy-mm-dd hh:mm:ss'"} );
				attributesAndTypes.add( new String[] {"EndTime", "TIMESTAMP 'yyyy-mm-dd hh:mm:ss'"} );
				attributesAndTypes.add( new String[] {"Status", "VARCHAR(20)"} );
				attributesAndTypes.add( new String[] {"Artifacts", "VARCHAR(45)"} );
			} break;
			
			case "NODESASSIGNEDTO":
			{
				attributesAndTypes.add( new String[] {"TID", "INT"} );
				attributesAndTypes.add( new String[] {"NodeName", "VARCHAR(45)"} );
			} break;
			
			case "IPSASSIGNEDTO":
			{
				attributesAndTypes.add( new String[] {"TID", "INT"} );
				attributesAndTypes.add( new String[] {"IP", "VARCHAR(15)"} );
			} break;
			
			case "CLIENTSASSIGNEDTO":
			{
				attributesAndTypes.add( new String[] {"TID", "INT"} );
				attributesAndTypes.add( new String[] {"ClientName", "VARCHAR(45)"} );
			} break;
			
			case "SUITEASSIGNEDTO":
			{
				attributesAndTypes.add( new String[] {"TID", "INT"} );
				attributesAndTypes.add( new String[] {"SID", "INT"} );
			} break;
			
			default:
			{
				System.out.println("Invalid table name! Unable to determine attributes and their corresponding types!\n"); // Add extra new line
			} break;
		}*/
        
        // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
        if( tableName.equals("NODES") )
        {
            attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"Type", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"Location", "VARCHAR(20)"} );
            attributesAndTypes.add( new String[] {"Model", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"TenG_IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"OneG_IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"iDRAC_IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"CacheCard", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"Available", "BOOLEAN ('True' or 'False')"} );
        }
        else if( tableName.equals("CLIENTS") )
        {
            attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"Type", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"Location", "VARCHAR(20)"} );
            attributesAndTypes.add( new String[] {"TenG_IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"OneG_IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"iDRAC_IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"OS", "VARCHAR(20)"} );
            attributesAndTypes.add( new String[] {"Available", "BOOLEAN ('True' or 'False')"} );
        }
        else if( tableName.equals("IPADDRESSES") )
        {
            attributesAndTypes.add( new String[] {"IP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"Available", "BOOLEAN ('True' or 'False')"} );
        }
        else if( tableName.equals("TESTSUITES") )
        {
            attributesAndTypes.add( new String[] {"SID", "INT"} );
            attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
        }
        else if( tableName.equals("TESTCASES") )
        {
            attributesAndTypes.add( new String[] {"CID", "INT"} );
            attributesAndTypes.add( new String[] {"SID", "INT"} );
            attributesAndTypes.add( new String[] {"Name", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"MinNumClusters", "INT"} );
            attributesAndTypes.add( new String[] {"MinNumNodes", "INT"} );
            attributesAndTypes.add( new String[] {"MinNumClients", "INT"} );
            attributesAndTypes.add( new String[] {"ClusterOption", "VARCHAR(45)"} );
            attributesAndTypes.add( new String[] {"vNode", "BOOLEAN ('True' or 'False')"} );
            attributesAndTypes.add( new String[] {"EstimatedRunTime", "TIME 'hh:mm:ss'"} );
            attributesAndTypes.add( new String[] {"LastExecution", "TIMESTAMP 'yyyy-mm-dd hh:mm:ss'"} );
        }
        else if( tableName.equals("TESTEXECUTIONSETS") )
        {
            attributesAndTypes.add( new String[] {"TID", "INT"} );
            attributesAndTypes.add( new String[] {"IsRunning", "BOOLEAN ('True' or 'False')"} );
            attributesAndTypes.add( new String[] {"MVIP", "VARCHAR(15)"} );
            attributesAndTypes.add( new String[] {"SVIP", "VARCHAR(15)"} );
        }
        else if( tableName.equals("TESTRESULTS") )
        {
            attributesAndTypes.add( new String[] {"TestResultID", "INT"} );
            attributesAndTypes.add( new String[] {"StartTime", "TIMESTAMP 'yyyy-mm-dd hh:mm:ss'"} );
            attributesAndTypes.add( new String[] {"EndTime", "TIMESTAMP 'yyyy-mm-dd hh:mm:ss'"} );
            attributesAndTypes.add( new String[] {"Status", "VARCHAR(20)"} );
            attributesAndTypes.add( new String[] {"Artifacts", "VARCHAR(45)"} );
        }
        else if( tableName.equals("NODESASSIGNEDTO") )
        {
            attributesAndTypes.add( new String[] {"TID", "INT"} );
            attributesAndTypes.add( new String[] {"NodeName", "VARCHAR(45)"} );
        }
        else if( tableName.equals("IPSASSIGNEDTO") )
        {
            attributesAndTypes.add( new String[] {"TID", "INT"} );
            attributesAndTypes.add( new String[] {"IP", "VARCHAR(15)"} );
        }
        else if( tableName.equals("CLIENTSASSIGNEDTO") )
        {
            attributesAndTypes.add( new String[] {"TID", "INT"} );
            attributesAndTypes.add( new String[] {"ClientName", "VARCHAR(45)"} );
        }
        else if( tableName.equals("SUITEASSIGNEDTO") )
        {
            attributesAndTypes.add( new String[] {"TID", "INT"} );
            attributesAndTypes.add( new String[] {"SID", "INT"} );
        }
        else
        {
            System.out.println("Invalid table name! Unable to determine attributes and their corresponding types!\n"); // Add extra new line
        }
        
        return(attributesAndTypes);
	}



	// -------------------------------------------------------------------- QUERY HANDLERS --------------------------------------------------------------------

	// Handle the query regarding the equipment in the database
	public static void HandleAvailableEquipmentQuery(Connection conn) throws SQLException
	{
        Statement stmt = conn.createStatement();
		Scanner keyboardScanner = new Scanner(System.in);
		
		// BEGIN - HANDLE SELECT/FROM CLAUSE
		
		String equipment;
		String selectClause = "";
		
        int validEntry = 1;
		do
		{
			System.out.println("The equipment is:");
			System.out.println(" - Nodes");
			System.out.println(" - Clients");
			System.out.println(" - IPAddresses");
			System.out.println("\n");
			System.out.print("Enter in the equipment type that you wish to inquire about (i.e. Nodes, Clients, IPAddresses): ");
			
			equipment = keyboardScanner.nextLine();
			// Make upper case to standardize the switch statement cases
			equipment.toUpperCase();
			
			// Build the select clause for query                    TODO: Add more? Add all? Add user desired selection?
			/*switch(equipment)                                     NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
			{
				case "NODES":
				{
					selectClause = "Name as NodeName, " +
								   "Type as NodeType, " +
								   "Model as NodeModel, " +
								   "Available";
				} break;
				
				case "CLIENTS":
				{
					selectClause = "Name as ClientName, " +
								   "Type as ClientType, " +
								   "Available";
				} break;
				
				case "IPADDRESSES":
				{
					selectClause = "IP, " +
								   "Available";
				} break;
				
				default:
				{
					System.out.println("Invalid equipment type! Please try again.\n");
					validEntry = 0;
				} break;
			}*/
            
            // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
            if( equipment.equals("NODES") )
            {
                selectClause = "Name as NodeName, " +
								   "Type as NodeType, " +
								   "Model as NodeModel, " +
								   "Available";
            }
            else if( equipment.equals("CLIENTS") )
            {
                selectClause = "Name as ClientName, " +
								   "Type as ClientType, " +
								   "Available";
            }
            else if( equipment.equals("IPADDRESSES") )
            {
                selectClause = "IP, " + 
                                    "Available";
            }
            else
            {
                System.out.println("Invalid equipment type! Please try again.\n");
				validEntry = 0;
            }
            
		} while( validEntry != 1 );
        
		
		// The attributes and types that the desired table has
		ArrayList<String[]> attributesAndTypes = BuildAttributesAndTypesArray(equipment);
		
		String formattedAggregationInformation = ""; // Initialize to empty string
		
        validEntry = 1;
		do
		{
			System.out.println("Would you like to add an aggregation function to the select clause? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The aggregation operations to perform based on what the user wants
				ArrayList<String[]> aggregationInformation;
				
				// Call function to prompt user for aggregation information given attribtue names and types
				aggregationInformation = GetAggregationInfo(attributesAndTypes);
				
				// Call function to format the aggregation information into an appropriate select cluase addition string for our query
				formattedAggregationInformation = FormatAggregationOperationsForQuery(aggregationInformation);
			}
			// User does not wish to provide an aggregation addition to the select clause
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the formattedAggregationInformation portion to add nothing to our string later when adding to select clause
				formattedAggregationInformation = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// If we have something in the select clause already then we need to add a comma to separate the entries
		if( selectClause != null && !selectClause.isEmpty() )
		{
			selectClause += ", ";
		}
		
		// Add whatever formatted aggregation information that there might be (empty string adds nothing, i.e. if no aggregation information was entered)
		selectClause += formattedAggregationInformation;
		
		
		// END - HANDLE SELECT/FROM CLAUSE
		
		
		
		// BEGIN - HANDLE WHERE CLAUSE
		
		String whereClause = ""; // Initialize to empty string
		
        validEntry = 1;
		do
		{
			System.out.println("Would you like to filter the results? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The filtered array of our attributes, values, and types based on what the user wants
				ArrayList<String[]> filteredAttributesValuesAndTypes;
				
				// Call function to prompt user for filtering information given attribtue names and types
				filteredAttributesValuesAndTypes = GetFilterValues(attributesAndTypes);
				
				// Call function to format the filtered attribtues, values, and types into an appropriate where cluase string for our query
				whereClause = FormatFilteredValuesForQuery(filteredAttributesValuesAndTypes);
			}
			// User does not wish to provide a filter
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the whereClause portion to an empty string to be tested for before posting query
				whereClause = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// END - HANDLE WHERE CLAUSE
		
		
		
		// BEGIN - HANDLE GROUP BY CLAUSE
		
		String groupByClause = ""; // Initialize to empty string
		
        validEntry = 1;
		do
		{
			System.out.println("Would you like to add a group by clause? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The aggregation operations to perform based on what the user wants
				ArrayList<String> groupByInformation;
				
				// Call function to prompt user for group by clause information given attribtue names and types
				groupByInformation = GetGroupByInfo(attributesAndTypes);
				
				// Call function to format the group by information into an appropriate group by cluase for our query
				groupByClause = FormatGroupByClauseForQuery(groupByInformation);
			}
			// User does not wish to provide a filter
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the groupByClause portion to an empty string to be tested for before posting query
				groupByClause = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// END - HANDLE GROUP BY CLAUSE
		
		
		
		
		// BEGIN - HANDLE QUERY EXECUTION
		
		// Build that query!
		String qry = "select " + selectClause + " from " + equipment;
		
		// Do we have a where clause to add on?
		if( whereClause != null && !whereClause.isEmpty() )
		{
			qry += " where " + whereClause;
		}
		
		// Do we have a group by clause to add on?
		if( groupByClause != null && !groupByClause.isEmpty() )
		{
			qry += " group by " + groupByClause;
		}
		
		ResultSet rs = stmt.executeQuery(qry);
		// Step 3: loop through the result set
		System.out.println("Name\tMajor");
		while (rs.next())
		{
			// TODO: Check spacing in output
			/*switch(equipment)                                     NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
			{
				case "NODES":
				{
					String nodeName = rs.getString("NodeName");
					String nodeType = rs.getString("NodeType");
					String nodeModel = rs.getString("NodeModel");
					String available = Boolean.toString(rs.getBoolean("Available"));
					System.out.println(nodeName + "\t" + nodeType + "\t" + nodeModel + "\t" + available);
				} break;
				
				case "CLIENTS":
				{
					String clientName = rs.getString("ClientName");
					String clientType = rs.getString("ClientType");
					String available = Boolean.toString(rs.getBoolean("Available"));
					System.out.println(clientName + "\t" + clientType + "\t" + available);
				} break;
				
				case "IPADDRESSES":
				{
					String ip = rs.getString("IP");
					String available = Boolean.toString(rs.getBoolean("Available"));
					System.out.println(ip + "\t" + available);
				} break;
				
				default:
				{
					System.out.println("Invalid equipment type!\n");
					throw new SQLException("Invalid equipment type entry from user"); // TODO: Need a better exception here
				} break;
			}*/
            
            // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
            if( equipment.equals("NODES") )
            {
                String nodeName = rs.getString("NodeName");
                String nodeType = rs.getString("NodeType");
                String nodeModel = rs.getString("NodeModel");
                String available = Boolean.toString(rs.getBoolean("Available"));
                System.out.println(nodeName + "\t" + nodeType + "\t" + nodeModel + "\t" + available);
            }
            else if( equipment.equals("CLIENTS") )
            {
                String clientName = rs.getString("ClientName");
                String clientType = rs.getString("ClientType");
                String available = Boolean.toString(rs.getBoolean("Available"));
                System.out.println(clientName + "\t" + clientType + "\t" + available);
            }
            else if( equipment.equals("IPADDRESSES") )
            {
                String ip = rs.getString("IP");
                String available = Boolean.toString(rs.getBoolean("Available"));
                System.out.println(ip + "\t" + available);
            }
            else
            {
                System.out.println("Invalid equipment type!\n");
				throw new SQLException("Invalid equipment type entry from user"); // TODO: Need a better exception here
            }
            
		}
		System.out.println( );
		rs.close();
		
		// END - HANDLE QUERY EXECUTION
	}

		

		// Handle the query regarding the test metadata in the database
	public static void HandleTestMetadataQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		Scanner keyboardScanner = new Scanner(System.in);
		
		// BEGIN - HANDLE SELECT/FROM CLAUSE
		
		String testMetadata;
		String selectClause = "";
		
        int validEntry = 1;
		do
		{
			System.out.println("The types of test metadata are:");
			System.out.println(" - TestSuites");
			System.out.println(" - TestCases");
			System.out.println(" - TestExecutionSets");
			System.out.println(" - TestResults");
			System.out.println("\n");
			System.out.print("Enter in the type of test metadata that you wish to inquire about (i.e. TestSuites, TestCases, TestExecutionSets, TestResults): ");
			
			testMetadata = keyboardScanner.nextLine();
			// Make upper case to standardize the switch statement cases
			testMetadata.toUpperCase();
			
			// Build the select clause for query                    TODO: Add more? Add all? Add user desired selection?
			/*switch(testMetadata)                                  NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
			{
				case "TESTSUITES":
				{
					selectClause = "SID as TestSuiteID, " +
								   "Name as TestSuiteName";
				} break;
				
				case "TESTCASES":
				{
					selectClause = "CID as TestCaseID, " +
								   "SID as TestSuiteID, " +
								   "Name as TestCaseName, " +
								   "EstimatedRuntime, " +
								   "LastExecution";
				} break;
				
				case "TESTEXECUTIONSETS":
				{
					selectClause = "TID as TestExecutionSetID, " +
								   "IsRunning, " +
								   "MVIP, " +
								   "SVIP";
				} break;
				
				case "TESTRESULTS":
				{
					selectClause = "TestResultID, " +
								   "StartTime, " +
								   "EndTime, " +
								   "Status, " +
								   "Artifacts";
				} break;
				
				default:
				{
					System.out.println("Invalid test metadata type! Please try again.");
					System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
					validEntry = 0;
				} break;
			}*/
            
            // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
            if( testMetadata.equals("TESTSUITES") )
            {
                selectClause = "SID as TestSuiteID, " +
								   "Name as TestSuiteName";
            }
            else if( testMetadata.equals("TESTCASES") )
            {
                selectClause = "CID as TestCaseID, " +
								   "SID as TestSuiteID, " +
								   "Name as TestCaseName, " +
								   "EstimatedRuntime, " +
								   "LastExecution";
            }
            else if( testMetadata.equals("TESTEXECUTIONSETS") )
            {
                selectClause = "TID as TestExecutionSetID, " +
								   "IsRunning, " +
								   "MVIP, " +
								   "SVIP";
            }
            else if( testMetadata.equals("TESTRESULTS") )
            {
                selectClause = "TestResultID, " +
								   "StartTime, " +
								   "EndTime, " +
								   "Status, " +
								   "Artifacts";
            }
            else
            {
                System.out.println("Invalid test metadata type! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
            }
            
		} while( validEntry != 1 );
		
		// The attributes and types that the desired table has
		ArrayList<String[]> attributesAndTypes = BuildAttributesAndTypesArray(testMetadata);
		
		
		String formattedAggregationInformation = ""; // Initialize to empty string
		
        validEntry = 1;
		do
		{
			System.out.println("Would you like to add an aggregation function to the select clause? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The aggregation operations to perform based on what the user wants
				ArrayList<String[]> aggregationInformation;
				
				// Call function to prompt user for aggregation information given attribtue names and types
				aggregationInformation = GetAggregationInfo(attributesAndTypes);
				
				// Call function to format the aggregation information into an appropriate select cluase addition string for our query
				formattedAggregationInformation = FormatAggregationOperationsForQuery(aggregationInformation);
			}
			// User does not wish to provide an aggregation addition to the select clause
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the formattedAggregationInformation portion to add nothing to our string later when adding to select clause
				formattedAggregationInformation = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// If we have something in the select clause already then we need to add a comma to separate the entries
		if( selectClause != null && !selectClause.isEmpty() )
		{
			selectClause += ", ";
		}
		
		// Add whatever formatted aggregation information that there might be (empty string adds nothing, i.e. if no aggregation information was entered)
		selectClause += formattedAggregationInformation;
		
		
		// END - HANDLE SELECT/FROM CLAUSE
		
		
		
		
		// BEGIN - HANDLE WHERE CLAUSE
		
		String whereClause = ""; // Initialize to empty string
        validEntry = 1;
		do
		{
			System.out.println("Would you like to filter the results? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The filtered array of our attributes, values, and types based on what the user wants
				ArrayList<String[]> filteredAttributesValuesAndTypes;
				
				// Call function to prompt user for filtering information given attribtue names and types
				filteredAttributesValuesAndTypes = GetFilterValues(attributesAndTypes);
				
				// Call function to format the filtered attribtues, values, and types into an appropriate where cluase string for our query
				whereClause = FormatFilteredValuesForQuery(filteredAttributesValuesAndTypes);
			}
			// User does not wish to provide a filter
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the whereClause portion to an empty string to be tested for before posting query
				whereClause = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// END - HANDLE WHERE CLAUSE
		
		
		
		// BEGIN - HANDLE GROUP BY CLAUSE
		
		String groupByClause = ""; // Initialize to empty string
		
		validEntry = 1;
		do
		{
			System.out.println("Would you like to add a group by clause? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The aggregation operations to perform based on what the user wants
				ArrayList<String> groupByInformation;
				
				// Call function to prompt user for group by clause information given attribtue names and types
				groupByInformation = GetGroupByInfo(attributesAndTypes);
				
				// Call function to format the group by information into an appropriate group by cluase for our query
				groupByClause = FormatGroupByClauseForQuery(groupByInformation);
			}
			// User does not wish to provide a filter
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the groupByClause portion to an empty string to be tested for before posting query
				groupByClause = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// END - HANDLE GROUP BY CLAUSE
		
		

		
		// HANDLE QUERY EXECTION
		
		// Build that query!
		String qry = "select " + selectClause + " from " + testMetadata;
		
		// Do we have a where clause to add on?
		if( whereClause != null && !whereClause.isEmpty() )
		{
			qry += " where " + whereClause;
		}
		
		// Do we have a group by clause to add on?
		if( groupByClause != null && !groupByClause.isEmpty() )
		{
			qry += " group by " + groupByClause;
		}
		
		ResultSet rs = stmt.executeQuery(qry);
		// Step 3: loop through the result set
		System.out.println("Name\tMajor");
		while (rs.next())
		{
			// TODO: Check spacing in output
			// Based on the testMetadata value, print out the appropriate output
			/*switch(testMetadata)                                     NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
			{
				case "TESTSUITES":
				{
					int testSuiteID = rs.getInt("TestSuiteID");
					String testSuiteName = rs.getString("TestSuiteName");
					System.out.println(testSuiteID + "\t" + testSuiteName);
				} break;
				
				case "TESTCASES":
				{
					int testCaseID = rs.getInt("TestCaseID");
					int testSuiteID = rs.getInt("TestSuiteID");
					String testCaseName = rs.getString("TestCaseName");
					java.sql.Time estimatedRuntime = rs.getTime("EstimatedRuntime");
					String lastExecution = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rs.getTimestamp("LastExecution"));
					System.out.println(Integer.toString(testCaseID) + "\t" + Integer.toString(testSuiteID) + "\t" + testCaseName + "\t" + estimatedRuntime.toString() + "\t" + lastExecution);
				} break;
				
				case "TESTEXECUTIONSETS":
				{
					int testExecutionSetID = rs.getInt("TestExecutionSetID");
					String isRunning = Boolean.toString(rs.getBoolean("IsRunning"));
					String mvip = rs.getString("MVIP");
					String svip = rs.getString("SVIP");
					System.out.println(Integer.toString(testExecutionSetID) + "\t" + isRunning + "\t" + mvip + "\t" + svip);
				} break;
				
				case "TESTRESULTS":
				{
					int testResultID = rs.getInt("TestResultID");
					String startTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rs.getTimestamp("StartTime"));
					String endTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rs.getTimestamp("EndTime"));
					String status = rs.getString("Status");
					String artifacts = rs.getString("Artifacts");
                    
					System.out.println(Integer.toString(testResultID) + "\t" + startTime + "\t" + endTime + "\t" + status + "\t" + artifacts);
				} break;
				
				default:
				{
					System.out.println("Invalid test metadata type!");
					throw new SQLException("Invalid test metadata type entry from user"); // TODO: Need a better exception here
				} break;
			}*/
            
            // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
            if( testMetadata.equals("TESTSUITES") )
            {
                int testSuiteID = rs.getInt("TestSuiteID");
                String testSuiteName = rs.getString("TestSuiteName");
                System.out.println(testSuiteID + "\t" + testSuiteName);
            }
            else if( testMetadata.equals("TESTCASES") )
            {
                int testCaseID = rs.getInt("TestCaseID");
                int testSuiteID = rs.getInt("TestSuiteID");
                String testCaseName = rs.getString("TestCaseName");
                java.sql.Time estimatedRuntime = rs.getTime("EstimatedRuntime");
                String lastExecution = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rs.getTimestamp("LastExecution"));
                System.out.println(Integer.toString(testCaseID) + "\t" + Integer.toString(testSuiteID) + "\t" + testCaseName + "\t" + estimatedRuntime.toString() + "\t" + lastExecution);
            }
            else if( testMetadata.equals("TESTEXECUTIONSETS") )
            {
                int testExecutionSetID = rs.getInt("TestExecutionSetID");
                String isRunning = Boolean.toString(rs.getBoolean("IsRunning"));
                String mvip = rs.getString("MVIP");
                String svip = rs.getString("SVIP");
                System.out.println(Integer.toString(testExecutionSetID) + "\t" + isRunning + "\t" + mvip + "\t" + svip);
            }
            else if( testMetadata.equals("TESTRESULTS") )
            {
                int testResultID = rs.getInt("TestResultID");
                String startTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rs.getTimestamp("StartTime"));
                String endTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rs.getTimestamp("EndTime"));
                String status = rs.getString("Status");
                String artifacts = rs.getString("Artifacts");
                
                System.out.println(Integer.toString(testResultID) + "\t" + startTime + "\t" + endTime + "\t" + status + "\t" + artifacts);
            }
            else
            {
                System.out.println("Invalid test metadata type!");
                throw new SQLException("Invalid test metadata type entry from user"); // TODO: Need a better exception here
            }
            
		}
		System.out.println( );
		rs.close();
		
		// END - HANDLE QUERY EXECUTION
	}

		

	// Handle the query regarding the assignments in the database
	public static void HandleAssignmentQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		Scanner keyboardScanner = new Scanner(System.in);
		
		// BEGIN - HANDLE SELECT/FROM CLAUSES
		
		String assignment;
		String selectClause = "";
		
        int validEntry = 1;
		do
		{
			System.out.println("The assignment types are:");
			System.out.println(" - NodesAssignedTo");
			System.out.println(" - IPsAssignedTo");
			System.out.println(" - ClientsAssignedTo");
			System.out.println(" - SuiteAssignedTo");
			System.out.println("\n");
			System.out.print("Enter in the type of assignment type that you wish to inquire about (i.e. NodesAssignedTo, IPsAssignedTo, ClientsAssignedTo, SuiteAssignedTo): ");
			
			assignment = keyboardScanner.nextLine();
			// Make upper case to standardize the switch statement cases
			assignment.toUpperCase();
			
			// Build the select clause for query                    TODO: Add more? Add all? Add user desired selection?
			/*switch(assignment)                                    NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
			{
				case "NODESASSIGNEDTO":
				{
					selectClause = "TID as TestExecutionSetID, " +
								   "NodeName";
				} break;
				
				case "IPSASSIGNEDTO":
				{
					selectClause = "TID as TestExecutionSetID, " +
								   "IP";
				} break;
				
				case "CLIENTSASSIGNEDTO":
				{
					selectClause = "TID as TestExecutionSetID, " +
								   "ClientName";
				} break;
				
				case "SUITEASSIGNEDTO":
				{
					selectClause = "TID as TestExecutionSetID, " +
								   "SID as TestSuiteID";
				} break;
				
				default:
				{
					System.out.println("Invalid assignment type! Please try again.");
					System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
					validEntry = 0;
				} break;
			}*/
            
            // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
            if( assignment.equals("NODESASSIGNEDTO") )
            {
                selectClause = "TID as TestExecutionSetID, " +
								   "NodeName";
            }
            else if( assignment.equals("IPSASSIGNEDTO") )
            {
                selectClause = "TID as TestExecutionSetID, " +
								   "IP";
            }
            else if( assignment.equals("CLIENTSASSIGNEDTO") )
            {
                selectClause = "TID as TestExecutionSetID, " +
								   "ClientName";
            }
            else if( assignment.equals("SUITEASSIGNEDTO") )
            {
                selectClause = "TID as TestExecutionSetID, " +
								   "SID as TestSuiteID";
            }
            else
            {
                System.out.println("Invalid assignment type! Please try again.");
                System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
                validEntry = 0;
            }
            
		} while( validEntry != 1 );
		
		// The attributes and types that the desired table has
		ArrayList<String[]> attributesAndTypes = BuildAttributesAndTypesArray(assignment);
		
		
		String formattedAggregationInformation = ""; // Initialize to empty string
		
        validEntry = 1;
		do
		{
			System.out.println("Would you like to add an aggregation function to the select clause? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The aggregation operations to perform based on what the user wants
				ArrayList<String[]> aggregationInformation;
				
				// Call function to prompt user for aggregation information given attribtue names and types
				aggregationInformation = GetAggregationInfo(attributesAndTypes);
				
				// Call function to format the aggregation information into an appropriate select cluase addition string for our query
				formattedAggregationInformation = FormatAggregationOperationsForQuery(aggregationInformation);
			}
			// User does not wish to provide an aggregation addition to the select clause
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the formattedAggregationInformation portion to add nothing to our string later when adding to select clause
				formattedAggregationInformation = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// If we have something in the select clause already then we need to add a comma to separate the entries
		if( selectClause != null && !selectClause.isEmpty() )
		{
			selectClause += ", ";
		}
		
		// Add whatever formatted aggregation information that there might be (empty string adds nothing, i.e. if no aggregation information was entered)
		selectClause += formattedAggregationInformation;
		
		
		// END - HANDLE SELECT/FROM CLAUSES
		
		
		
		
		// BEGIN - HANDLE WHERE CLAUSE
		
		String whereClause = ""; // Initialize to empty string
        validEntry = 1;
		do
		{
			System.out.println("Would you like to filter the results? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The filtered array of our attributes, values, and types based on what the user wants
				ArrayList<String[]> filteredAttributesValuesAndTypes;
				
				// Call function to prompt user for filtering information given attribtue names and types
				filteredAttributesValuesAndTypes = GetFilterValues(attributesAndTypes);
				
				// Call function to format the filtered attribtues, values, and types into an appropriate where cluase string for our query
				whereClause = FormatFilteredValuesForQuery(filteredAttributesValuesAndTypes);
			}
			// User does not wish to provide a filter
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the whereClause portion to an empty string to be tested for before posting query
				whereClause = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// END - HANDLE WHERE CLAUSE
		
		
		
		// BEGIN - HANDLE GROUP BY CLAUSE
		
		String groupByClause = ""; // Initialize to empty string
		
        validEntry = 1;
		do
		{
			System.out.println("Would you like to add a group by clause? (Y)es or (N)o: ");
			String response = keyboardScanner.nextLine();
			response.toUpperCase();
			
			if( response.equals("Y") || response.equals("YES") )
			{
				// The aggregation operations to perform based on what the user wants
				ArrayList<String> groupByInformation;
				
				// Call function to prompt user for group by clause information given attribtue names and types
				groupByInformation = GetGroupByInfo(attributesAndTypes);
				
				// Call function to format the group by information into an appropriate group by cluase for our query
				groupByClause = FormatGroupByClauseForQuery(groupByInformation);
			}
			// User does not wish to provide a filter
			else if( response.equals("N") || response.equals("NO") )
			{
				// Set the groupByClause portion to an empty string to be tested for before posting query
				groupByClause = "";
			}
			// The response was not understood, let's try again
			else
			{
				System.out.println("Invalid entry! Please try again.");
				System.out.println("-----------------------------------------\n"); // TODO: Does this look nice?
				validEntry = 0;
			}
		} while( validEntry != 1 );
		
		// END - HANDLE GROUP BY CLAUSE
		
		
		
		
		// BEGIN - HANDLE QUERY EXECUTION
		
		// Build that query!
		String qry = "select " + selectClause + " from " + assignment;
		
		// Do we have a where clause to add on?
		if( whereClause != null && !whereClause.isEmpty() )
		{
			qry += " where " + whereClause;
		}
		
		// Do we have a group by clause to add on?
		if( groupByClause != null && !groupByClause.isEmpty() )
		{
			qry += " group by " + groupByClause;
		}
		
		//Execute the query
		ResultSet rs = stmt.executeQuery(qry);
		// Step 3: loop through the result set
		System.out.println("Name\tMajor");
		while (rs.next())
		{
			// TODO: Check spacing in output
			// Based on the assignment selected, grab the output tuples' values
			/*switch(assignment)                                     NOTE: Apparently switch statements with strings don't work on derby? Says that it requires an int
			{
				case "NODESASSIGNEDTO":
				{
					int testExecutionSetID = rs.getInt("TestExecutionSetID");
					String nodeName = rs.getString("NodeName");
					System.out.println(nodeName + "\t" + Integer.toString(testExecutionSetID));
				} break;
				
				case "IPSASSIGNEDTO":
				{
					int testExecutionSetID = rs.getInt("TestExecutionSetID");
					String ip = rs.getString("IP");
					System.out.println(ip + "\t" + Integer.toString(testExecutionSetID));
				} break;
				
				case "CLIENTSASSIGNEDTO":
				{
					int testExecutionSetID = rs.getInt("TestExecutionSetID");
					String clientName = rs.getString("ClientName");
					System.out.println(clientName + "\t" + Integer.toString(testExecutionSetID));
				} break;
				
				case "SUITEASSIGNEDTO":
				{
					int testExecutionSetID = rs.getInt("TestExecutionSetID");
					int testSuiteID = rs.getInt("TestSuiteID");
					System.out.println(Integer.toString(testSuiteID) + "\t" + Integer.toString(testExecutionSetID));
				} break;
				
				default:
				{
					System.out.println("Invalid assignment type! Please try again.");
					throw new SQLException("Invalid assignment type entry from user"); // TODO: Need a better exception here
				} break;
			}*/
            
            // NOTE: The below if statement is a workaround to the switch statement on the derby servers not accepting anything but integers in switch statements
            if( assignment.equals("NODESASSIGNEDTO") )
            {
                int testExecutionSetID = rs.getInt("TestExecutionSetID");
                String nodeName = rs.getString("NodeName");
                System.out.println(nodeName + "\t" + Integer.toString(testExecutionSetID));
            }
            else if( assignment.equals("IPSASSIGNEDTO") )
            {
                int testExecutionSetID = rs.getInt("TestExecutionSetID");
                String ip = rs.getString("IP");
                System.out.println(ip + "\t" + Integer.toString(testExecutionSetID));
            }
            else if( assignment.equals("CLIENTSASSIGNEDTO") )
            {
                int testExecutionSetID = rs.getInt("TestExecutionSetID");
                String clientName = rs.getString("ClientName");
                System.out.println(clientName + "\t" + Integer.toString(testExecutionSetID));
            }
            else if( assignment.equals("SUITEASSIGNEDTO") )
            {
                int testExecutionSetID = rs.getInt("TestExecutionSetID");
                int testSuiteID = rs.getInt("TestSuiteID");
                System.out.println(Integer.toString(testSuiteID) + "\t" + Integer.toString(testExecutionSetID));
            }
            else
            {
                System.out.println("Invalid assignment type! Please try again.");
				throw new SQLException("Invalid assignment type entry from user"); // TODO: Need a better exception here
            }
            
		}
		System.out.println( );
		rs.close();
		
		// END - HANDLE QUERY EXECUTION
	}		
}    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
