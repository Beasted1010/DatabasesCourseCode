


insert into TestExecutionSets values
    (1, True, true, '111.111.111.110', '111.111.111.120', 11),
    (2, True, true, '111.111.111.111', '111.111.111.121', 12),
    (3, True, true, '111.111.111.112', '111.111.111.122', 13),
    (4, True, true, '111.111.111.113', '111.111.111.123', 14),
    (5, True, true, '111.111.111.114', '111.111.111.124', 15);
insert into IPAddresses values
    ('111.111.111.110', True), -- MVIPs
    ('111.111.111.111', True),
    ('111.111.111.112', True),
    ('111.111.111.113', True),
    ('111.111.111.114', True),
    ('111.111.111.120', True), -- SVIPs
    ('111.111.111.121', True),
    ('111.111.111.122', True),
    ('111.111.111.123', True),
    ('111.111.111.124', True),
    ('111.111.111.130', True), -- 10G
    ('111.111.111.131', True),
    ('111.111.111.132', True),
    ('111.111.111.133', True),
    ('111.111.111.134', True),
    ('111.111.111.140', True), -- 1G
    ('111.111.111.141', True),
    ('111.111.111.142', True),
    ('111.111.111.143', True),
    ('111.111.111.144', True),
    ('111.111.111.150', True), -- iDRAC
    ('111.111.111.151', True),
    ('111.111.111.152', True),
    ('111.111.111.153', True),
    ('111.111.111.154', True);
insert into IPAssignments values
    ('111.111.111.111', 1),
    ('111.111.111.112', 2),
    ('111.111.111.113', 3),
    ('111.111.111.114', 4),
    ('111.111.111.115', 5);
insert into Nodes values
    ('NodeA', 'sf905', 'ICT', 'model1', '111.111.111.130', '111.111.111.140', '111.111.111.150', 'Windows', 'cacheCard1', True),
    ('NodeB', 'sf906', 'WIC', 'model2', '111.111.111.131', '111.111.111.141', '111.111.111.151', 'Linux', 'cacheCard2', True),
    ('NodeC', 'sf906', 'BOULDER', 'model1', '111.111.111.132', '111.111.111.142', '111.111.111.152', 'Linux', 'cacheCard1', True);
insert into NodesAssignedTo values
    ('NodeA', 1),
    ('NodeB', 2),
    ('NodeC', 3);
insert into Clients values
    ('ClientA', 'clientT1', '111.111.111.130', '111.111.111.140', '111.111.111.150', 'Windows', True),
    ('ClientB', 'clientT2', '111.111.111.131', '111.111.111.141', '111.111.111.151', 'Linux', True),
    ('ClientC', 'clientT1', '111.111.111.132', '111.111.111.142', '111.111.111.152', 'Windows', True),
insert into ClientsAssignedTo values
    ('ClientA', 1),
    ('ClientB', 2),
    ('ClientC', 3);
insert into TestResults values
    (11, 1, '1111-11-11 11:11:11', '1111-11-22 11:11:11', 'Pass', 'arti1'), -- First attribute is TestIDNumber
    (12, 2, '1111-12-11 11:11:11', '1111-12-22 11:11:11', 'Fail', 'arti1'),
    (13, 2, '1111-13-11 11:11:11', '1111-13-22 11:11:11', 'Pass', 'arti2');
insert into TestSuites values
    (21), -- First attribute is SID (SuiteID)... Only 1... What is the point of this?
    (22),
    (23);
insert into SuiteAssignedTo values
    (1, 21), -- First attribute is TID (Test ID)
    (2, 22),
    (3, 23);
insert into TestCases values
    (31, 21, 1, 2, 3, True, 11:11:11, 1111-11-22 11:11:11), -- First attribute is TCID (Test Case ID)
    (32, 22, 1, 2, 3, True, 11:11:11, 1111-12-22 11:11:11),
    (33, 23, 1, 2, 3, True, 11:11:11, 1111-13-22 11:11:11);
insert into SVIPAssignedTo values
    ('111.111.111.120', 1),
    ('111.111.111.121', 2),
    ('111.111.111.121', 2);
insert into MVIPAssignedTo values
    ('111.111.111.110', 1),
    ('111.111.111.111', 2),
    ('111.111.111.112', 3);


































