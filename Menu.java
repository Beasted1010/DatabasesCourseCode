// File: Menu.java

// This program llustrates the use of a menu, which would be the basis
// for constructing a larger program by adding more options where each
// option is handled by a separate function.

import java.sql.*;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;
import org.apache.derby.jdbc.ClientDriver;

public class Menu
{
	public static void main(String[] args)
	{
		int choice;
		Connection conn = null;
		try
		{
			// Step 1: get a client object and connect to the database server
			Driver d = new ClientDriver();
			String url = "jdbc:derby://localhost:6521/../atscheduler" 
						  + ";create=false";
			conn = d.connect(url, null);
			
            boolean done = false;
            while(!done)
            {
                // Make a menu selection
                choice = PrintMenuAndGetResponse();
                switch (choice)
                {
                    case 0: done = true;
                            break; //case for quitting the program without running a query
                    case 1: AvailableClientNodeQuery(conn);
                            break;
                    case 2: AddNode(conn);
                            break;
                    case 3: UpdateNode(conn);
                            break;
                    case 4: NodeInformationInputQuery(conn);
                            break;
                    case 5: NodeAssignmentInputQuery(conn);
                            break;
                    case 6: TestCaseInformationQuery(conn);
                            break;
                    case 7: NumberOfRunningTestSuitesQuery(conn);
                            break;
                    case 8: NumberOfPassingTestCasesQuery(conn);
                            break;
                    case 9: AvailableIPAddressesQuery(conn);
                            break;
                    case 10: DeleteNode(conn);
                            break;
                    default: System.out.println("Illegal choice");
                             break;
                }
            }
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			// Step 4: Disconnect from the server
			try
			{
				if(conn != null)
				conn.close();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	// Queries ================================================================================
	
	// This method is for the query of names of nodes or clients currently available.
	public static void AvailableClientNodeQuery(Connection conn) throws SQLException
	{
		Scanner keyboardScanner = new Scanner(System.in);
		System.out.print("Enter the equipment type that you would like to see available [Nodes|Clients]: ");
		String equipment = keyboardScanner.nextLine();
		
		Statement stmt = conn.createStatement();
		String qry = "select Name "
					+ "from " + equipment
					+ " where available = true";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Step 3: loop through the result set
		System.out.println("Available " + equipment);
		System.out.println("----------------------");
		while (rs.next())
		{
			String name = rs.getString("Name");
			System.out.println(name);
		}
		System.out.println( );
		rs.close();
	}
	
	// This method is for the query of the IP addresses currently available.
	public static void AvailableIPAddressesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select IP "
					+ "from IPAddresses"
					+ " where available = true";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Step 3: loop through the result set
		System.out.println("Available IP Addresses");
		System.out.println("----------------------");
		while (rs.next())
		{
			String name = rs.getString("IP");
			System.out.println(name);
		}
		System.out.println( );
		rs.close();
	}
	
	// This method is for adding a new node
	public static void AddNode(Connection conn) throws SQLException
	{
		Map<String, String> nodeInfo = InputNodeInfo();
		
		Statement stmt = conn.createStatement();
		String qry = "insert into Nodes (name, type, location, model, teng_ip, oneg_ip, idrac_ip, cachecard, available) "
					+ "values ("
					+ "\'" + nodeInfo.get("name") + "\', \'"
					+ nodeInfo.get("type") + "\', \'"
					+ nodeInfo.get("location") + "\', \'"
					+ nodeInfo.get("model") + "\', \'"
					+ nodeInfo.get("teng_ip") + "\', \'"
					+ nodeInfo.get("oneg_ip") + "\', \'"
					+ nodeInfo.get("idrac_ip") + "\', \'"
					+ nodeInfo.get("cachecard") + "\',"
					+ "true)";
		stmt.executeUpdate(qry);
		System.out.println("Node " + nodeInfo.get("name") + " added successfully.");
	}
	
	// This method is for updating a current node
	public static void UpdateNode(Connection conn) throws SQLException
	{
		Scanner keyboardScanner = new Scanner(System.in);
		System.out.print("Enter the exact name of the node that you would like to update: ");
		String name = keyboardScanner.nextLine();
		System.out.print("Enter attribute to update [name|type|location|model|teng_ip|oneg_ip|idrac_ip|cachecard|available]: ");
		String attribute = keyboardScanner.nextLine();
		System.out.print("Enter the new value: ");
		String new_val = keyboardScanner.nextLine();
		
		Statement stmt = conn.createStatement();
		String qry = "update Nodes "
					+ "set " + attribute + "= \'" + new_val + "\' "
					+ "where name = \'" + name + "\'";
		stmt.executeUpdate(qry);
		System.out.println("Node " + name + " updated successfully.");
	}
    
    // This method is for deleting a node
	public static void DeleteNode(Connection conn) throws SQLException
	{
		Scanner keyboardScanner = new Scanner(System.in);
		System.out.print("Enter the exact name of the node that you would like to delete: ");
		String name = keyboardScanner.nextLine();
		
		Statement stmt = conn.createStatement();
		String qry = "delete from Nodes where name = \'" + name + "\'";
		stmt.executeUpdate(qry);
		System.out.println("Node " + name + " deleted successfully.");
	}

		// This method is for the query for the information regarding a user specified node.
	public static void NodeInformationInputQuery(Connection conn) throws SQLException
	{
		// Build and execute the query
		String nodeName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter a node name: ");
		nodeName = scannerObject.nextLine( );
		Statement stmt = conn.createStatement();
		String qry = "select Name as NodeName, Type, Location, Model, TenG_IP, OneG_IP, iDRAC_IP, CacheCard, Available "
						+ "from Nodes "
						+ "where Name = \'" + nodeName + "\'";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
        System.out.format("%-10s %-5s %-10s %-10s %-18s %-18s %-18s %-15s %-10s \n", "NodeName", "Type", "Location", "Model", "TenG_IP", "OneG_IP", "iDRAC_IP", "CacheCard", "Available");
		while (rs.next())
		{
			String nodeNameOut = rs.getString("NodeName");
			String type = rs.getString("Type");
			String location = rs.getString("Location");
			String model = rs.getString("Model");
			String tenG_IP = rs.getString("TenG_IP");
			String oneG_IP = rs.getString("OneG_IP");
			String iDRAC_IP = rs.getString("iDRAC_IP");
			String cacheCard = rs.getString("CacheCard");
			String available = rs.getString("Available");
			System.out.format("%-10s %-5s %-10s %-10s %-18s %-18s %-18s %-15s %-10s \n", nodeNameOut, type, location, model, tenG_IP, oneG_IP, iDRAC_IP, cacheCard, available);
		}
        System.out.println();
		rs.close();
	}
	
	
	// This method is for the query for information about a node's assignment
	public static void NodeAssignmentInputQuery(Connection conn) throws SQLException
	{
		// Build and execute the query
		String nodeName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter a node name: ");
		nodeName = scannerObject.nextLine( );
		Statement stmt = conn.createStatement();
		
		String qry = "select NodeName, TID as TestSuiteNodeAssignedTo "
						+ "from NodesAssignedTo "
						+ "where NodeName = \'" + nodeName + "\'";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
		System.out.format("%-25s %-10s \n", "TestSuiteNodeAssignedTo", "NodeName");
		while (rs.next())
		{
			int testSuiteNodeAssignedTo = rs.getInt("TestSuiteNodeAssignedTo");
			String nodeNameOut = rs.getString("NodeName");
			System.out.format( "%-25s %-10s \n", Integer.toString(testSuiteNodeAssignedTo), nodeNameOut );
		}
		System.out.println( );
		rs.close();
	}
	
	// This method is for the query for information regarding a specific test case
	public static void TestCaseInformationQuery(Connection conn) throws SQLException
	{
		String testCaseName;
		Scanner scannerObject = new Scanner(System.in);
		System.out.print("Enter a test case name: ");
		testCaseName = scannerObject.nextLine( );
		
		Statement stmt = conn.createStatement();
		String qry = "select CID, Name as TestCaseName, SID, EstimatedRuntime "
						+ "from TestCases "
						+ "where Name = \'" + testCaseName + "\'";
		ResultSet rs = stmt.executeQuery(qry);
		
		// Loop through the result set
        System.out.format("%-5s %-30s %-5s %-15s \n", "CID", "TestCaseName", "SID", "EstimatedRuntime");
		while (rs.next())
		{
			int cID = rs.getInt("CID");
			String testCaseNameOut = rs.getString("TestCaseName");
			int sID = rs.getInt("SID");
			java.sql.Time estimatedRuntime = rs.getTime("EstimatedRuntime"); // TODO: This may need a better data type
            System.out.format("%-5s %-30s %-5s %-15s \n", Integer.toString(cID), testCaseNameOut, Integer.toString(sID), estimatedRuntime.toString() );
		}
		System.out.println( );
		rs.close();
	}
	
	
		// This method is for the query for the number of passing test cases
	public static void NumberOfPassingTestCasesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select count(TestResultID) as NumPassingTestCases "
						+ "from TestResults "
						+ "where status = 'passed' "
						+ "group by status";
		ResultSet rs = stmt.executeQuery(qry);
		
        int numPassingTestCases = 0;
		// Loop through the result set
		System.out.println("NumPassingTestCases");
		// Should only be one row
        while (rs.next())
		{
			numPassingTestCases = rs.getInt("NumPassingTestCases");
		}
		
        System.out.println( Integer.toString(numPassingTestCases) );
        
		System.out.println( );
		rs.close();
	}


	// This method is for the query for the number of running test suites
	public static void NumberOfRunningTestSuitesQuery(Connection conn) throws SQLException
	{
		Statement stmt = conn.createStatement();
		String qry = "select count(TID) as NumRunningTestSuites "
						+ "from TestExecutionSets "
						+ "where IsRunning = True "
						+ "group by IsRunning";
		ResultSet rs = stmt.executeQuery(qry);
		
        int numRunningTestSuites = 0;
		// Loop through the result set
		System.out.println("NumRunningTestSuites");
        // Should only be one row
		while (rs.next())
		{
			numRunningTestSuites = rs.getInt("NumRunningTestSuites");
		}
		System.out.println( Integer.toString(numRunningTestSuites) );
        
		System.out.println( );
		rs.close();
	}



	
	// Helpers ================================================================================
	
	// Requests Node info from user
	public static Map<String, String> InputNodeInfo()
	{
		Map<String, String> nodeInfo = new HashMap<String, String>();
		nodeInfo.put("name", "");
		nodeInfo.put("type", "");
		nodeInfo.put("location", "");
		nodeInfo.put("model", "");
		nodeInfo.put("teng_ip", "");
		nodeInfo.put("oneg_ip", "");
		nodeInfo.put("idrac_ip", "");
		nodeInfo.put("cachecard", "");
		nodeInfo.put("available", "false");

		Scanner keyboardScanner = new Scanner(System.in);
		System.out.print("Enter Name: ");
		nodeInfo.put("name", keyboardScanner.nextLine());
		System.out.print("Enter type: ");
		nodeInfo.put("type", keyboardScanner.nextLine());
		System.out.print("Enter location: ");
		nodeInfo.put("location", keyboardScanner.nextLine());
		System.out.print("Enter model: ");
		nodeInfo.put("model", keyboardScanner.nextLine());
		System.out.print("Enter 10G IP: ");
		nodeInfo.put("teng_ip", keyboardScanner.nextLine());
		System.out.print("Enter 1G IP: ");
		nodeInfo.put("oneg_ip", keyboardScanner.nextLine());
		System.out.print("Enter iDRAC IP: ");
		nodeInfo.put("idrac_ip", keyboardScanner.nextLine());
		System.out.print("Enter cachecard: ");
		nodeInfo.put("cachecard", keyboardScanner.nextLine());
		
		return nodeInfo;
	}
    
	
	public static int PrintMenuAndGetResponse()
	{
		Scanner keyboardScanner = new Scanner(System.in);
		int response;
		
		System.out.println("Choose from one of the following options:");
		System.out.println(" 0. Quit the program\n");
		System.out.println(" 1. List available Nodes/Clients.");
		System.out.println(" 2. Add Node.");
		System.out.println(" 3. Update Node.");
		System.out.println(" 4. View Node Details By Name.");
		System.out.println(" 5. View Node Test Suite Assignment.");
		System.out.println(" 6. View Test Case Details By Name.");
		System.out.println(" 7. Count Running Test Suites.");
		System.out.println(" 8. Count Passing Test Cases.");
	    System.out.println(" 9. List available IP Addresses.");
        System.out.println(" 10. Delete Node.");
		System.out.print("Your choice ==> ");
		response = keyboardScanner.nextInt();
		System.out.println( );
		return response;
	}
    
}
