
insert into Nodes (name, type, location, model, teng_ip, oneg_ip, idrac_ip, cachecard, available) values
    ('NodeA', 'sf905', 'ICT', 'model1', '111.111.111.160', '111.111.111.170', '111.111.111.180', 'cacheCard1', True),
    ('NodeB', 'sf906', 'WIC', 'model2', '111.111.111.161', '111.111.111.171', '111.111.111.181', 'cacheCard2', True),
    ('NodeC', 'sf906', 'BOULDER', 'model1', '111.111.111.162', '111.111.111.172', '111.111.111.182', 'cacheCard1', False);
	
insert into Clients (name, type, location, teng_ip, oneg_ip, idrac_ip, os, available) values
    ('ClientA', 'clientT1', 'ICT', '111.111.111.163', '111.111.111.173', '111.111.111.183', 'Windows', True),
    ('ClientB', 'clientT2', 'Boulder', '111.111.111.164', '111.111.111.174', '111.111.111.184', 'Linux', False),
    ('ClientC', 'clientT1', 'WIC', '111.111.111.165', '111.111.111.175', '111.111.111.185', 'Windows', True);

insert into IPAddresses (ip, available) values
    ('111.111.111.110', True),
    ('111.111.111.111', True),
    ('111.111.111.112', True),
    ('111.111.111.113', True),
    ('111.111.111.114', False),
    ('111.111.111.120', True),
    ('111.111.111.121', True),
    ('111.111.111.122', True),
    ('111.111.111.123', True),
    ('111.111.111.124', True),
    ('111.111.111.130', False),
    ('111.111.111.131', True),
    ('111.111.111.132', True),
    ('111.111.111.133', True),
    ('111.111.111.134', True),
    ('111.111.111.140', True),
    ('111.111.111.141', True),
    ('111.111.111.142', True),
    ('111.111.111.143', True),
    ('111.111.111.144', True),
    ('111.111.111.150', True),
    ('111.111.111.151', True),
    ('111.111.111.152', True),
    ('111.111.111.153', True),
    ('111.111.111.154', True);
	
insert into TestSuites (sid, name) values
    (21, 'suite1'),
    (22, 'suite2'),
    (23, 'suite3');
	
insert into TestCases (cid, sid, name, minnumclusters, minnumnodes, minnumclients, clusteroption, vnode, estimatedruntime, lastexecution) values
    (31, 21, 'case1', 1, 2, 2, NULL, True, '16:01:11', '2017-11-22 18:24:16'),
    (32, 22, 'case2', 1, 2, 3, '--option: 1', True, '1:21:13', '2017-12-11 15:56:23'),
    (33, 23, 'case3', 1, 2, 1, '--option: 2', True, '3:42', '2018-01-15 22:16:14');
	
	

insert into TestExecutionSets(tid, isrunning, mvip, svip) values
    (1, False, '111.111.111.110', '111.111.111.120'),
    (2, False, '111.111.111.111', '111.111.111.121'),
    (3, False, '111.111.111.112', '111.111.111.122'),
    (4, True, '111.111.111.113', '111.111.111.123'),
    (5, True, '111.111.111.114', '111.111.111.124');
	
insert into TestResults (testresultid, starttime, endtime, status, artifacts) values
    (1, '2017-11-22 01:11:06', '2017-11-22 18:24:16', 'Pass', 'arti1'), 
    (2, '2017-12-11 14:34:20', '2017-12-11 15:56:23', 'Fail', 'arti1'),
    (3, '2018-01-15 19:20:13', '2018-01-15 22:16:14', 'Pass', 'arti2');


insert into NodesAssignedTo (tid, nodename) values
    (1, 'NodeA'),
    (1, 'NodeB'),
	(2, 'NodeA'),
	(2, 'NodeB'),
	(2, 'NodeC'),
	(3, 'NodeB'),
	(3, 'NodeC'),
	(4, 'NodeA'),
	(4, 'NodeC'),
	(5, 'NodeA'),
	(5, 'NodeB');

insert into IPsAssignedTo (tid, ip) values
    (1, '111.111.111.153'),
    (5, '111.111.111.154');

insert into ClientsAssignedTo (tid, clientname) values
    (1, 'ClientA'),
	(1, 'ClientC'),
	(1, 'ClientB'),
	(2, 'ClientA'),
	(2, 'ClientB'),
	(2, 'ClientC'),
	(3, 'ClientA'),
	(3, 'ClientB');

insert into SuiteAssignedTo (tid, sid) values
    (1, 21),
	(1, 22), 
    (2, 22),
    (3, 23), 
	(4, 21),
	(5, 22);



























